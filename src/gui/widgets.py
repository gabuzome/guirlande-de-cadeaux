from abc import ABC, abstractmethod
from collections import defaultdict

import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox as msg

class DefaultFrame(tk.Frame, ABC):
    row_counter = 0

    def __init__(self, notebook: ttk.Notebook, title: str, *args, **kwargs):
        super().__init__(
            master=notebook,
            *args, **kwargs
        )
        notebook.add(self, text=title)

        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

    @abstractmethod
    def draw_frame(self):
        pass

    @property
    def main_window(self):
        # Frame -> Notebook -> Main Windows
        return self.master.master


# Adapté de https://stackoverflow.com/a/47928390
class EntryPlaceholder(tk.Entry):
    """\
    la classe permet de simuler un *placeholder*, une indication pour le texte.
    De plus, il est possible de limier l'entrée à des chiffres seulement.
    """
    def __init__(self, master=None, is_digit=False, placeholder_tuple=None, placeholder_color='grey', *args, **kwargs):
        super().__init__(master, *args, **kwargs)

        self.placeholder = None
        self.default_value = None
        if placeholder_tuple:
            self.placeholder = placeholder_tuple[0]
            self.default_value = placeholder_tuple[1]
    
        self.placeholder_color = placeholder_color
        self.default_fg_color = self['fg']

        self.bind("<FocusIn>", self.foc_in)
        self.bind("<FocusOut>", self.foc_out)

        self.placeholder_here = False
        if self.get() == '' and self.placeholder:
            self.put_placeholder()

        self.is_digit = is_digit
        if is_digit:
            vcmd = self.register(self.validate_int_only)
            self.configure(validate='all', validatecommand=(vcmd, '%P'))

    @property
    def value(self) -> str:
        if self.placeholder_here:
            if self.default_value:
                return self.default_value
            else:
                return ''
            
        val = self.get()
        return int(val) if self.is_digit else val
    
    @value.setter
    def value(self, val: str | int):
        """Modifier la valeur de l'entry, vérifier si entier donné si `is_digit`"""
        val = str(val)
        if self.is_digit and not val.isdigit():
            return
        self.delete('0', 'end')
        self.insert(0, val)
        self.placeholder_here = False
        self['fg'] = self.default_fg_color

    @value.deleter
    def value(self):
        self.delete('0', 'end')
        self.foc_out()


    def change_placeholder(self, placeholder:str|None):
        self.placeholder = placeholder
        if self.placeholder_here:
            del self.value

    # Tout le bazar qui suit permet de gérer la modification de l'entrée tout
    # en garantissant que le placeholder s'affiche toujours au bon moment et
    # que la couleur soit bonne.

    def put_placeholder(self):
        """On met le placeholder et on l'indique"""
        self.placeholder_here = True
        self.insert(0, self.placeholder)
        self['fg'] = self.placeholder_color

    def foc_in(self, *args):
        """Quand on gagne le focus, si le placeholder est là on le supprime"""
        if self.placeholder_here:
            self.delete('0', 'end')
            self['fg'] = self.default_fg_color
            self.placeholder_here = False

    def foc_out(self, *args):
        """Quand on perd le focus, si l'Entry est vide on met le placeholder"""
        if not self.get() and self.placeholder:
            self.put_placeholder()

    def validate_int_only(self, P) -> bool:
        """Accepte tous les textes si le placeholder est activé, sinon que les chiffres"""
        if self.placeholder_here:
            return True
        str_P = str(P)
        if str_P.isdigit() or str_P == '':
            return True
        else:
            return False
        

class ToggleButton(ttk.Label):
    def __init__(self, master:tk.Widget, values, *, command=None, width=None):
        super().__init__(master, anchor='center', width=width)
        self._callback = command
        self.bind("<Button-1>", lambda _: self.toggle())
        self._values = values
        self._is_toggled = False

        self._update_style()

    def _update_style(self):
        if self._is_toggled:
            self.config(text=self._values[1], relief="sunken")
        else:
            self.config(text=self._values[0], relief="raised")

    @property
    def value(self):
        return self._is_toggled
    
    @value.setter
    def value(self, val):
        self._is_toggled = val
        self._update_style()

    @value.deleter
    def value(self):
        self.value = False

    def toggle(self):
        if 'disabled' in self.state():
            return
        self.value = not self.value
        if self._callback is not None:
            self._callback()


class CheckButtonRight(tk.Frame):
    """Une classe inutile pour avoir la case à cocher *à droite* du texte"""
    def __init__(self, master=None, text='', variable=None, command=None, *args, **kwargs):
        super().__init__(master=master, *args, **kwargs)

        self.variable = variable
        self.command = command

        self.checkbutton = ttk.Checkbutton(
            self,
            text='',
            variable=self.variable,
            command=self.command,
            takefocus=0,
        )
        self.checkbutton.grid(row=0, column=1)
        self.label = ttk.Label(self, text=text)
        self.label.grid(row=0, column=0)
        self.label.bind("<Enter>", lambda _:self.enter())
        self.label.bind("<Leave>", lambda _:self.leave())
        self.label.bind("<Button 1>", lambda _:self.toggle())

    def get(self) -> bool:
        return self.checkbutton.get()
    
    def enter(self):
        pass
    def leave(self):
        pass

    def toggle(self):
        if self.variable:
            self.variable.set(not self.variable.get())
        if self.command:
            self.command()


# D'après https://stackoverflow.com/a/61253373
def set_state_to_children(widget: tk.Widget, enabled: bool):
    """Permet de "désactiver" tous les widgets enfants récursivement"""

    wtype = widget.winfo_class()
    if wtype in ('TSeparator'):
        return
    elif wtype not in ('Frame','Labelframe','TFrame','TLabelframe'):
        widget.configure(state = 'normal' if enabled else 'disabled')
    else:
        for child in widget.winfo_children():
            set_state_to_children(child, enabled)


class LabelFormPairCreator:
    """Comme ça on ne s'embête pas à gérer manuellement le row_count"""
    def __init__(self, master: tk.Widget, initial_row_count:int=0, length_text:int=40, length_digit:int=20) -> None:
        self.master = master
        self.row_count = initial_row_count
        self.length_text = length_text
        self.length_digit = length_digit

    def create_label_entry_pair(self, text: str, hint: str | tuple | None = None, digit_only: bool = False, is_password: bool = False, *, width=None):
        """Crée une paire Label/EntryPlaceholder et retourne le EntryPlaceholder

        Le EntryPlaceholder est automatiquement placé à la ligne
        """
        if type(hint) is str:
            hint=(hint, '')
        label = ttk.Label(self.master, text=text+' :', width=width, anchor='e')
        label.grid(column=0, row=self.row_count, sticky='e', pady=2)
        widget = EntryPlaceholder(
            self.master,
            placeholder_tuple=hint,
            is_digit=digit_only,
            show='*' if is_password else '',
            width = self.length_digit if digit_only else self.length_text,
        )
        widget.grid(column=1, row=self.row_count, sticky='w')
        self.row_count += 1
        return label, widget
    
    def create_label_toggle_pair(self, text: str, values, *, command=None, width=None):
        """Crée une paire Label/ToggleButton et retourne le ToggleButton

        Le ToggleButton est automatiquement placé à la ligne
        """
        label = ttk.Label(self.master, text=text+' :')
        label.grid(column=0, row=self.row_count, sticky='e')
        widget = ToggleButton(
            self.master,
            values,
            command=command,
            width=width,
        )
        widget.grid(column=1, row=self.row_count, sticky='w')
        self.row_count += 1
        return label, widget
    
    def add_separator(self):
        sep = ttk.Separator(self.master, orient='horizontal')
        sep.grid(row=self.row_count, column=0, columnspan=2, sticky='ew', pady=20)
        self.row_count += 1

class ListboxWithForms(ttk.Frame):
    def __init__(self, master: tk.Widget, title: str, default_name:str = '', default_listbox_values=[], on_form_updated_callback=None):
        super().__init__(master)

        self._was_constructed = False
        self._widgets_to_construct_list = []
        self._on_form_updated_callback = on_form_updated_callback

        self.title = title
        self.default_name = default_name

        # Données :
        self.listbox = None # La Listbox
        self.listbox_variable = tk.Variable(value=default_listbox_values) # Les lignes affichées par la listbox
        self._listbox_current_index = -1
        self._listbox_current_item = None

        self._label_dict = {} # Le dictionnaire str -> Label des champs du formulaire
        self._widget_dict = {} # Le dictionnaire str -> Widget des champs du formulaire
        self.principal_entry_key = None # L'entrée dont la valeur modifie le champ de la listbox
        self.data = defaultdict(dict) # les données contenues par tous les champs

        # Misc
        self._creator = None # Le LabelFormPairCreator

    def get_widget(self, key: str):
        return self._widget_dict.get(key, None)
    
    def get_label(self, key: str):
        return self._label_dict.get(key, None)

    def construct(self):
        """Procède aux ultimes vérifications et construit le widget"""
        if self._was_constructed:
            raise Exception("Déjà construite !")

        self.columnconfigure(2, weight=1)
        self.rowconfigure(0, weight=1)

        # Partie gauche : ListBox et Boutons
        frame_listbox_button = ttk.Frame(self)
        frame_listbox_button.grid(row=0, column=0, sticky='nes')
        frame_listbox_button.rowconfigure(0, weight=1)

        frame_listbox = ttk.Frame(frame_listbox_button)
        frame_listbox.grid(row=0, column=0, sticky='news')
        frame_listbox.rowconfigure(0, weight=1)
        frame_listbox.columnconfigure(0, weight=1)

        scrollbar = ttk.Scrollbar(frame_listbox)
        self.listbox = tk.Listbox(frame_listbox, yscrollcommand=scrollbar.set, listvariable=self.listbox_variable, width=30)
        self.listbox.bind('<<ListboxSelect>>', lambda _: self._on_listbox_selected())
        scrollbar.configure(command=self.listbox.yview)

        self.listbox.grid(row=0, column=0, sticky='news')
        scrollbar.grid(row=0, column=1, sticky='news')

        frame_buttons = ttk.Frame(frame_listbox_button)
        frame_buttons.grid(row=1, column=0, sticky='news')

        button_add = ttk.Button(frame_buttons, text="Ajouter", command=self._on_button_add_clicked)
        button_add.grid(row=0, column=0)
        button_del = ttk.Button(frame_buttons, text="Supprimer", command=self._on_button_del_clicked)
        button_del.grid(row=0, column=1)

        ttk.Separator(self, orient='vertical').grid(row=0, column=1, sticky='ns')

        # Partie droite : Voir les détails dans un formulaire
        self.frame_forms = ttk.Frame(self)
        self.frame_forms.grid(row=0, column=2, sticky='new', padx=3, pady=3)
        self.frame_forms.columnconfigure((0,1), weight=1)

        ttk.Label(self.frame_forms, text=self.title, justify='center').grid(row=0, column=0, columnspan=2, pady=10)
        self._creator = LabelFormPairCreator(self.frame_forms, length_text=40, initial_row_count=1)

        for params in self._widgets_to_construct_list:
            widget_type = params[0]
            params = params[1:]
            if widget_type == 'entry':
                self._construct_entry(*params)
            elif widget_type == 'sep':
                self._creator.add_separator()
            elif widget_type == 'toggle':
                self._construct_toggle_button(*params)
            else:
                raise Exception("Type invalide")
        del self._widgets_to_construct_list

        if self.principal_entry_key is None:
            raise Exception("Il faut une entrée principale avant de construire.")

        self._was_constructed = True
        set_state_to_children(self.frame_forms, False)

    @property
    def listbox_selected_index(self):
        sel = self.listbox.curselection()
        if len(sel) == 0:
            return None
        else:
            return sel[0]
        
    @property
    def listbox_selected_item(self):
        index = self.listbox_selected_index
        if index is None:
            return None
        data = self.listbox_variable.get()
        return data[self.listbox_selected_index]
    
    
    def add_entry(self, key: str, label_text: str, entry_place_holder: str='', principal: bool=False, width=None):
        if self._was_constructed:
            raise Exception("Déjà construite !")
        self._widgets_to_construct_list.append(['entry', key, label_text, entry_place_holder, principal, width])

    def add_separator(self):
        if self._was_constructed:
            raise Exception("Déjà construite !")
        self._widgets_to_construct_list.append(['sep'])

    def add_toggle_button(self, key: str, text: str, values, command=None, width=None):
        if self._was_constructed:
            raise Exception("Déjà construite !")
        self._widgets_to_construct_list.append(['toggle', key, text, values, command, width])

    def _construct_entry(self, key: str, label_text: str, entry_place_holder: str='', principal: bool=False, width=None):
        label, widget = self._creator.create_label_entry_pair(label_text, entry_place_holder, width=width)
        self._label_dict[key] = label
        self._widget_dict[key] = widget
        if principal:
            self._set_entry_as_principal(key)
        return widget
    
    def _construct_toggle_button(self, key: str, text, values, command, width):
        label, widget = self._creator.create_label_toggle_pair(text, values, command=command, width=width)
        self._label_dict[key] = label
        self._widget_dict[key] = widget
        return widget
    
    def _set_entry_as_principal(self, key: str):
        """On attache une des entries à ce qui est affiché dans la listbox"""

        if self.principal_entry_key is not None:
            raise Exception(f"Il y a déjà une entrée principale : {self.principal_entry_key}")
        if key not in self._widget_dict:
            raise Exception(f"Entrée invalide !")

        self.principal_entry_key = key
        self._widget_dict[key].bind("<FocusOut>", lambda e: self._update_listbox_row_item(), add="+") # Ne pas overrider le foc_out

        
    def _on_listbox_selected(self):
        index = self.listbox_selected_index
        if index is None:
            return
        
        if self._listbox_current_index == index and self._listbox_current_item == self.listbox_selected_item:
            return

        # Sauvegarde du contenu
        self._save_texts_in_dict()

        # MàJ données courantes
        self._listbox_current_index = index
        self._listbox_current_item = self.listbox_selected_item

        current_dict = self.data[self._listbox_current_item]
        print(f"Loading from {self._listbox_current_item}")

        for widget in self._widget_dict.values():
            del widget.value

        for key, widget in self._widget_dict.items():
            if val := current_dict.get(key) : widget.value = val

        if self._on_form_updated_callback is not None:
            self._on_form_updated_callback()

    def _save_texts_in_dict(self):
        if self._listbox_current_item is None: # au début
            return
        
        print(f"Saving in {self._listbox_current_item}")
        current_dict = self.data[self._listbox_current_item]
        for key, widget in self._widget_dict.items():
            current_dict[key] = widget.value

    def _update_listbox_row_item(self):
        principal_entry = self._widget_dict[self.principal_entry_key]
        newvalue = principal_entry.value
        if newvalue == '' or self._listbox_current_item == newvalue:
            return
        
        if newvalue in self.data:
            msg.showerror("Clé invalide", f"L'identifiant « {newvalue} » existe déjà. Veuillez en choisir un autre.")
            principal_entry.focus()
            return

        print(f"Passage de {self._listbox_current_item} à {newvalue}")
        data = self.data[self._listbox_current_item]
        del self.data[self._listbox_current_item]
        self._listbox_current_item = newvalue

        self.data[self._listbox_current_item] = data
        self.listbox.delete(self._listbox_current_index)
        self.listbox.insert(self._listbox_current_index, self._listbox_current_item)

    def _on_button_add_clicked(self):
        index = self.listbox_selected_index
        if index is None: index = 'end'
        else: index = index + 1
        nb = len(self.listbox_variable.get()) + 1
        name = f"{self.default_name} #{nb}"
        self.listbox.insert(index, name.strip())
        self.listbox.select_clear(0, 'end')
        self.listbox.select_set(index)
        set_state_to_children(self.frame_forms, True)
        self._on_listbox_selected()
        self._widget_dict[self.principal_entry_key].focus()

    def _on_button_del_clicked(self):
        index = self.listbox_selected_index
        if index is None: return
        key = self.listbox_selected_item
        self.listbox.delete(index)
        nb = len(self.listbox_variable.get())
        if nb == index: index -= 1
        self.listbox.select_clear(0, 'end')
        self.listbox.select_set(index)
        self.data.pop(key, None) # pop et pas del car parfois la clé n'existe pas, à l'ajout par ex
        if nb == 0:
            set_state_to_children(self.frame_forms, False)
        self._on_listbox_selected()
