import tkinter as tk
import tkinter.ttk as ttk

from . import widgets

temp_values = [f"Personne #{i}" for i in range(60)]

class FramePeople(widgets.DefaultFrame):
    """Fenêtre des gens"""
    def __init__(self, notebook, *args, **kwargs):
        super().__init__(notebook, "Personnes", *args, **kwargs)


    def draw_frame(self):
        main_frame = ttk.Frame(self)
        main_frame.grid(row=0, column=0, sticky='news')
        main_frame.rowconfigure(1, weight=1)
        main_frame.columnconfigure(0, weight=1)

        self.main_window.create_frame_io_config_people_group(main_frame)

        listbox_forms = widgets.ListboxWithForms(main_frame, "Informations sur les gens", default_name="Personne")
        listbox_forms.grid(row=1, column=0, sticky='news')
        
        listbox_forms.add_entry("unique_name", "Nom", "Nom unique identifiant la personne", principal=True)
        listbox_forms.add_entry("display_name", "Nom à afficher", "(Optionnel)")
        listbox_forms.add_entry("nickname", "Surnom", "(Optionnel)")
        listbox_forms.add_entry("email", "Adresse mail", "(Optionnel si pas d'envoi mail)")

        listbox_forms.add_separator()

        listbox_forms.add_entry('allowlist', "Liste de pioche", "Noms séparés par des virgules")
        listbox_forms.add_entry('allowlist_cluster', "Groupes de pioche", "Groupes séparés par des virgules")
        listbox_forms.add_entry('denylist', "Liste d'exclusion", "Noms séparés par des virgules")
        listbox_forms.add_entry('denylist_cluster', "Groupes d'exclusion", "Groupes séparés par des virgules")

        listbox_forms.construct()