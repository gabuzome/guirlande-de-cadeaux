import tkinter as tk
import tkinter.ttk as ttk

from . import widgets

class FrameClusters(widgets.DefaultFrame):
    """Fenêtre des groupes"""
    def __init__(self, notebook, *args, **kwargs):
        super().__init__(notebook, "Groupes", *args, **kwargs)

        self.button_mode = None
        self.entry_list = None
        self.label_list = None

    def draw_frame(self):
        main_frame = ttk.Frame(self)
        main_frame.grid(row=0, column=0, sticky='news')
        main_frame.rowconfigure(1, weight=1)
        main_frame.columnconfigure(0, weight=1)

        self.main_window.create_frame_io_config_people_group(main_frame)

        listbox_forms = widgets.ListboxWithForms(main_frame, "Création des groupes", default_name="Groupe", on_form_updated_callback=self._update_entry_list_placeholder)
        listbox_forms.grid(row=1, column=0, sticky='news')
        
        listbox_forms.add_entry("unique_name", "Groupe", "Nom unique identifiant le groupe", principal=True)
        listbox_forms.add_separator()
        listbox_forms.add_toggle_button("for_clusters", "Type de Groupe", ("Groupe de personnes", "Groupe de groupes"), width=20, command=self._update_entry_list_placeholder)
        listbox_forms.add_entry("list", '', width=20)
        listbox_forms.construct()

        self.button_mode = listbox_forms.get_widget('for_clusters')
        self.entry_list = listbox_forms.get_widget('list')
        self.label_list = listbox_forms.get_label('list')

        self._update_entry_list_placeholder()


    def _update_entry_list_placeholder(self):
        text_entry = "Groupes séparés par des virgules" if self.button_mode.value else "Personnes séparées par des virgules"
        self.entry_list.change_placeholder(text_entry)

        text_entry = "Liste des groupes :" if self.button_mode.value else "Listes des personnes :"
        self.label_list.config(text=text_entry)
