import tkinter as tk
import tkinter.ttk as ttk
import tkinter.font
from .clusters import FrameClusters
from .draw import FrameDraw
from .people import FramePeople

class MainWindow(tk.Tk):
    def __init__(self, scaling_factor, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.frame_height = 600
        self.frame_width = 800

        self.tk.call('tk', 'scaling', scaling_factor)
        self.title("Gᴜɪrlande de Cadeaux")

        self.style = ttk.Style()
        self.default_font = tkinter.font.nametofont('TkTextFont').actual()

        self.create_app()


    def run(self):
        # Empêche la fenêtre d'être redimensionnée trop petite
        self.withdraw() # cache la fenêtre le temps de recalculer sa taille
        self.update()
        self.minsize(int(self.winfo_width()*1.2), int(self.winfo_height()*1.1))
        self.maxsize(int(self.winfo_width()*2), int(self.winfo_height()*2))
        self.deiconify()

        self.mainloop()

    def create_app(self):
        tk.Grid.columnconfigure(self, 0, weight=1)
        tk.Grid.rowconfigure(self, 0, weight=1)

        notebook = ttk.Notebook(self)
        notebook.grid(column=0, row=0, sticky='news')

        FramePeople(notebook).draw_frame()
        FrameClusters(notebook).draw_frame()
        FrameDraw(notebook).draw_frame()

    def create_frame_io_config_people_group(self, master):
        main_frame_button = ttk.LabelFrame(master, text="Configuration des Personnes et des Groupes")
        main_frame_button.grid(row=0, column=0, sticky='news', padx=5, pady=5)
        main_frame_button.columnconfigure((0,1), weight=1)

        button_load = ttk.Button(main_frame_button, text="Charger", command=self.on_button_load_people_config_clicked)
        button_load.grid(row=0, column=0, sticky='e')
        button_save = ttk.Button(main_frame_button, text="Exporter", command=self.on_button_save_people_config_clicked)
        button_save.grid(row=0, column=1, sticky='w')

    def on_button_load_people_config_clicked(self):
        print("Chargement fichier conf")

    def on_button_save_people_config_clicked(self):
        print("Sauvegarde fichier conf")