import tkinter as tk
import tkinter.filedialog as tfd
import tkinter.messagebox as msg
import tkinter.ttk as ttk
import yaml

from dataclasses import dataclass
from . import widgets

@dataclass
class MailData:
    hostname    : widgets.EntryPlaceholder = None
    smtp_port   : widgets.EntryPlaceholder = None
    imap_port   : widgets.EntryPlaceholder = None
    username    : widgets.EntryPlaceholder = None
    password    : widgets.EntryPlaceholder = None
    sender_mail : widgets.EntryPlaceholder = None
    sender_name : widgets.EntryPlaceholder = None
    sent_folder : widgets.EntryPlaceholder = None

filetypes_yaml = (
    ("Fichiers YAML", "*.yaml"),
    ("Tous les fichiers", "*.*"),
)

filetypes_text = (
    ("Fichiers texte", "*.txt"),
    ("Tous les fichiers", "*.*"),
)
    
class FrameDraw(widgets.DefaultFrame):
    """Fenêtre du tirage"""
    def __init__(self, notebook, *args, **kwargs):
        super().__init__(notebook, "Tirage", *args, **kwargs)

        # Entrées texte pour config mail
        self.mail_data = MailData()

        # Widgets
        self.frame_mail = None
        self.frame_lezgo = None
        self.button_export_text = None
        self.entry_export_file = None
        
        # Autres xxxVars
        self.button_send_mail    = tk.BooleanVar(self,  True)
        self.button_show_draw    = tk.BooleanVar(self, False)
        self.button_export_draw  = tk.BooleanVar(self, False)
        self.save_password       = tk.BooleanVar(self,  True)
        self.text_file_to_export = tk.StringVar (self,    '')

    def draw_frame(self):
        """Peuple notre Frame de tirage"""

        ##### MAIL #####
        
        self.frame_mail = ttk.LabelFrame(self, text="Configuration Mail")
        self.frame_mail.grid(row=0, column=0, sticky='ews')
        self.frame_mail.columnconfigure(0, weight=1)

        frame_buttons = ttk.Frame(self.frame_mail)
        frame_buttons.columnconfigure(2, weight=1)
        frame_buttons.grid(row=0, column=0, sticky='news', pady=3)

        ttk.Button(frame_buttons, text="Charger", command=self.on_clicked_load_config).grid(column=0, row=0, sticky='w')
        ttk.Button(frame_buttons, text="Sauvegarder", command=self.on_clicked_save_config).grid(column=1, row=0, sticky='w')
        ttk.Checkbutton(frame_buttons, text="Exporter aussi le mot de passe mail", takefocus=0, var=self.save_password).grid(column=2, row=0, sticky='e')

        ttk.Separator(self.frame_mail, orient='horizontal').grid(row=1, column=0, sticky='we')

        frame_form = ttk.Frame(self.frame_mail)
        frame_form.grid(row=2, column=0, sticky='we', pady=3)

        frame_form.columnconfigure((0,1), weight=1)

        creator = widgets.LabelFormPairCreator(frame_form)
        _, self.mail_data.hostname    = creator.create_label_entry_pair("Serveur")
        _, self.mail_data.smtp_port   = creator.create_label_entry_pair("Port SMTP", hint=('Par défaut 587',587), digit_only=True)
        _, self.mail_data.imap_port   = creator.create_label_entry_pair("Port IMAP", hint=('Par défaut 143',143), digit_only=True)
        _, self.mail_data.username    = creator.create_label_entry_pair("Nom d'utilisateurice")
        _, self.mail_data.password    = creator.create_label_entry_pair("Mot de passe", is_password=True)
        _, self.mail_data.sender_mail = creator.create_label_entry_pair("Mail pour l'envoi")
        _, self.mail_data.sender_name = creator.create_label_entry_pair("Nom pour l'envoi")
        _, self.mail_data.sent_folder = creator.create_label_entry_pair("Dossier \"Envoyés\"", hint=('Par défaut "Sent"','Sent'))

        ##### MODALITÉS #####

        frame_draw = ttk.LabelFrame(self, text="Affichage du tirage")
        frame_draw.grid(row=1, column=0, sticky='news')
        frame_draw.columnconfigure(0, weight=1)
        frame_draw_mid = ttk.Frame(frame_draw)
        frame_draw_mid.grid(row=0, column=0, sticky='ns')

        widgets.CheckButtonRight(frame_draw_mid, text="Envoyer le tirage par mail", variable=self.button_send_mail, command=self.on_checkbutton_mail_toggled).grid(row=0, column=0, sticky='e')
        widgets.CheckButtonRight(frame_draw_mid, text="Afficher le Tirage", variable=self.button_show_draw, command=self.on_checkbuttons_changed).grid(row=1, column=0, sticky='e')
        widgets.CheckButtonRight(frame_draw_mid, text="Exporter le tirage dans un fichier", variable=self.button_export_draw, command=self.on_checkbuttons_export_text).grid(row=2, column=0, sticky='e')

        frame_export_file = ttk.Frame(frame_draw_mid)
        frame_export_file.grid(row=2, column=1, sticky='ew')
        frame_export_file.columnconfigure(1, weight=1)
        font_name = self.main_window.default_font['family']
        font_size = self.main_window.default_font['size']
        font_size_scaled = int(font_size*0.7)
        font_scaled = (font_name, font_size_scaled)

        self.button_export_text = ttk.Button(frame_export_file, text="Choisir", width=0, command=self.on_clicked_export_text, state = 'normal' if self.button_export_draw.get() else 'disabled')
        self.button_export_text.grid(row=0, column=0, sticky='w')
        self.entry_export_file = ttk.Entry(frame_export_file, textvariable=self.text_file_to_export, state='disabled', font=font_scaled, width=35)
        self.entry_export_file.grid(row=0, column=1, sticky='ns')

        ##### C'est Parti #####

        self.frame_lezgo = ttk.Frame(self, relief='groove', borderwidth=1)
        self.frame_lezgo.grid(row=2, column=0, sticky='news', pady=6)
        self.frame_lezgo.columnconfigure(0, weight=1)

        self.main_window.style.configure('Lezgo.TButton',
            foreground="maroon",
        )
        ttk.Button(self.frame_lezgo, text="Tirage !", style='Lezgo.TButton', command=self.on_clicked_lezgo).pack(pady=6)


    ##### Griser des parties au bon moment #####

    def on_checkbutton_mail_toggled(self):
        """Griser la partie mail si on n'envoie rien par mail"""
        widgets.set_state_to_children(self.frame_mail, self.button_send_mail.get())
        self.on_checkbuttons_changed()

    def on_checkbuttons_changed(self):
        """Griser la possibilité de faire un tirage si on n'en fait rien"""
        should_enable = self.button_export_draw.get() or self.button_send_mail.get() or self.button_show_draw.get()
        widgets.set_state_to_children(self.frame_lezgo, should_enable)

    def on_checkbuttons_export_text(self):
        """Griser la partie "choisir le ficher texte dans lequel exporter le tirage" """
        is_on = self.button_export_draw.get()
        widgets.set_state_to_children(self.button_export_text, is_on)
        if is_on and self.text_file_to_export.get() == '':
            self.on_clicked_export_text()
    
    
    ##### Évènements des clicks de boutons #####

    def on_clicked_load_config(self):
        name = tfd.askopenfilename(
            title="Charger un fichier de configuration mail",
            initialdir='.',
            filetypes=filetypes_yaml,
        )
        if name =='':
            return
        try:
            with open(name) as file:
                data = yaml.safe_load(file)
        except:
            msg.showerror("Fichier Invalide", "Le fichier n'a pas pu être ouvert. Est-ce bien du YAML valide ?")
        else:
            if data is None:
                return
            if val := data.get('hostname')    : self.mail_data.hostname   .value = val
            if val := data.get('smtp_port')   : self.mail_data.smtp_port  .value = val
            if val := data.get('imap_port')   : self.mail_data.imap_port  .value = val
            if val := data.get('username')    : self.mail_data.username   .value = val
            if val := data.get('password')    : self.mail_data.password   .value = val
            if val := data.get('sender_mail') : self.mail_data.sender_mail.value = val
            if val := data.get('sender_name') : self.mail_data.sender_name.value = val
            if val := data.get('sent_folder') : self.mail_data.sent_folder.value = val

    def on_clicked_save_config(self):
        name = tfd.asksaveasfilename(
            title="Sauvegarder comme fichier de configuration mail",
            initialdir='.',
            filetypes=filetypes_yaml,
        )
        if name == '':
            return
        if not name.endswith('.yaml') and not name.endswith('.yml'):
            name += '.yaml'
        with open(name, 'w', encoding='utf-8') as file:
            try:
                data = {}
                if val := self.mail_data.hostname    .value : data['hostname']    = val
                if val := self.mail_data.smtp_port   .value : data['smtp_port']   = val
                if val := self.mail_data.imap_port   .value : data['imap_port']   = val
                if val := self.mail_data.username    .value : data['username']    = val
                if self.save_password.get():
                    if val := self.mail_data.password.value : data['password']    = val
                if val := self.mail_data.sender_mail .value : data['sender_mail'] = val
                if val := self.mail_data.sender_name .value : data['sender_name'] = val
                if val := self.mail_data.sent_folder .value : data['sent_folder'] = val

                file.write("# Fichier de configuration mail pour l'outil ronde-de-cadeaux\n")
                file.write("# Généré avec https://framagit.org/gabuzome/guirlande-de-cadeaux\n\n") # autopromo :D
                yaml.dump(data, file, sort_keys=False)
            except:
                msg.showerror("Erreur", "Une erreur s'est produite lors de la sauvegarde du fichier :/")

    def on_clicked_export_text(self):
        name = tfd.asksaveasfilename(
            title="Exporter dans un fichier texte",
            initialdir='.',
            filetypes=filetypes_text,
        )
        if name == '':
            return
        else:
            if not name.endswith('.txt'):
                name += '.txt'
            self.text_file_to_export.set(name)
            self.entry_export_file.xview_moveto(1.) # montrer fin du path plutôt que début si path trop long

    def on_clicked_lezgo(self):
        print("tirage!")
