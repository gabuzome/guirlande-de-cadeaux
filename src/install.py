import importlib
import os
import pathlib
import subprocess
import sys

import tkinter.messagebox as msg

modules_required = [
    ['yaml', 'PyYAML'],
]

def install_missing_modules() -> bool:
    """Retourne vrai si tous les modules étaients installés avant l'appel à cette fonction"""
    modules = modules_required
    modules_to_install = []
    for module_import, module_pip in modules:
        try :
            importlib.import_module(module_import)
        except:
            modules_to_install.append(module_pip)
    if len(modules_to_install) == 0:
        return True
    
    message = f"""\
Les paquets suivants sont nécessaires pour le bon fonctionnement de l'application :
{ ", ".join(modules_to_install)}

En appuyant sur oui vous allez les installer automatiquement.
"""
    rep = msg.askquestion("Installation des paquets", message)
    if rep == 'yes':    
        try:
            subprocess.check_call([sys.executable, '-m', 'pip', 'install', *modules_to_install])
        except:
            msg.showerror("Erreur !", "Une erreur s'est produite, impossible d'installer les paquets nécessaires.")
        else:
            msg.showinfo("Succès !", "Les paquets nécessaires ont été installés.\n\nVeuillez relancer l'application.")

    return False

def initialize_submodule() -> bool:
    """Retourne faux si le sous-module `ronde-de-cadeaux` n'existait pas au lancement de la fonction"""

    here = pathlib.Path(__file__).parent / '../ronde-de-cadeaux'
    # On considère que le sous-module a été initialisé si le dossier correspondant n'est pas vide
    with os.scandir(str(here)) as it:
        if any(it):
            return True

    message = f"""\
Le sous-module ronde-de-cadeaux n'est pas initialisé.
En appuyant sur oui vous allez l'initialiser automatiquement.
"""
    rep = msg.askquestion("Initialisation des sous-modules", message)
    if rep == 'yes':
        try:
            subprocess.check_call(['git', 'submodule', 'init'])
            subprocess.check_call(['git', 'submodule', 'update'])
        except:
            msg.showerror("Erreur !", "Une erreur s'est produite, impossible d'initialiser les modules. git est-il bien installé ?")
        else:
            msg.showinfo("Succès !", "Les sous-modules ont été initialisés.\n\nVeuillez relancer l'application.")

    return False