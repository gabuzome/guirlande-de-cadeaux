import os
import pathlib
import platform

from src.install import install_missing_modules, initialize_submodule

# On vérifie que le sous-module est correctement initialisé et que les modules python sont installés

if not initialize_submodule():
    exit()

if not install_missing_modules():
    exit()

# Ensuite seulement on peut importer les autres modules

from src.gui.GUI import MainWindow

SCALING_FACTOR = 2 # 2 est la valeur "normale"

# Affichage plus joli pour les moniteurs à haut DPI sous Window$
if platform.uname().system == 'Windows':
    try:
        from ctypes import windll
        windll.shcore.SetProcessDpiAwareness(1)
    except:
        pass

# On se place dans le répertoire de ce fichier
path_main = str(pathlib.Path(__file__).parent)
os.chdir(path_main)



# Si tout va bien c'est parti !
app = MainWindow(SCALING_FACTOR)
app.run()