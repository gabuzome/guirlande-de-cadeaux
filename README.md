# Gᴜɪrlande de Cadeaux

Ce projet est une version graphique de l'outil de tirage aléatoire [ronde-de-cadeaux](https://framagit.org/bertille-ddp/ronde-de-cadeaux/).


Il est actuellement en développement, n'ayez-crainte ça finira par marcher

---

## TODO
- [ ] Rédiger le `README.md`
    - [ ] Texte
    - [ ] Images
- [x] Trouver un nom avec un jeu de mot génial
- [x] Auto install des modules et des paquets python
- [ ] GUI
    - [ ] Faire les onglets principaux
        - [ ] Personnes
        - [ ] Groupes
        - [x] Configuration
    - [ ] Brancher tout ça avec ronde-de-cadeaux
        - [ ] Fonctionnement de base
        - [ ] Système de logger pour interagir avec la sortie de ronde-de-cadeaux
    - [ ] Ajouter un nouvel onglet éditeur de message
